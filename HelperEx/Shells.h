#ifndef INC_Y1_Shell_h_
#define INC_Y1_Shell_h_ 1

#include <Helper/YModules.h>
#include YFM_Helper_GUIShell
#include YFM_Helper_ShellHelper
#include YFM_Helper_HostedUI
#include YFM_YCLib_Debug
#include <ystdex/path.hpp>
#include YFM_YSLib_Adaptor_Image // for YSLib::Drawing::HBitmap;
#include <ystdex/cache.hpp>
#include YFM_YSLib_UI_Loader
#include YFM_Helper_GUIApplication
#include <ystdex/operators.hpp>

namespace ystdex
{

/*!
\brief 树形关联容器深度优先搜索。
\since YSLib build 531
\todo 异常处理。
*/
//@{
template<class _type, typename _fCallable>
void
depth_first_traverse(const _type& root, _fCallable f, size_t depth = 0)
{
	f(root, depth);
	for(const auto& node : root)
	{
		ystdex::depth_first_traverse(node, f, depth + 1);
	}
}

template<class _type, typename _fCallable>
void
depth_first_traverse_n(const _type& root, _fCallable f, size_t max_depth,
	size_t depth = 0)
{
	if(depth < max_depth)
	{
		f(root, depth);
		for(const auto& node : root)
		{
			ystdex::depth_first_traverse_n(node, f, max_depth, depth + 1);
		}
	}
}
//@}

} // namespace ystdex;

namespace YSLib
{

namespace Drawing
{



} // namespace Drawing;

namespace UI
{

//! \since YSLib build 530
void
RegisterControls(WidgetLoader&);

} // namespace UI;

namespace Host
{

void
PrintFPS(FPSCounter&, Window&);

} // namespace Host;

//! \since YSLib build 454
template<typename _fCallable>
inline void
PostBoundTask(shared_ptr<Shell> h_shl, _fCallable&& f,
	Messaging::Priority prior = Messaging::NormalPriority)
{
	PostMessage<SM_Bound>(prior,
		make_pair(h_shl, Message(SM_Task, ValueObject(yforward(f)))));
}


namespace Shells
{

class AnimatedShell : public GUIShell
{
protected:
	FPSCounter fpsCounter{std::chrono::milliseconds(500)};

public:
	std::function<void(UI::PaintEventArgs&&)> Refresh;

	template<typename F>
	AnimatedShell(F&& f)
		: Refresh(yforward(f))
	{
		//	IdleSleep = {};
		IdleSleep = std::chrono::nanoseconds(10000000);
	}
};

} // namespace Shells;


//! \since YSLib build 433
using ValuePath = ystdex::path<vector<string>>;



#if 0
//! \since YSLib build 433
class YF_API ValuePathNorm : public ystdex::path_norm<string>
{
public:
	using value_type = string;

	PDefH(bool, is_delimiter, const value_type& str) override
		ImplRet(str == ".")

	PDefH(bool, is_parent, const value_type& str) ynothrow override
		ImplRet(str == "$$")

	PDefH(bool, is_root, const value_type& str) ynothrow override
		ImplRet(str == ".")

	PDefH(bool, is_self, const value_type& str) ynothrow override
		ImplRet(str == "$")

	DefClone(const override, ValuePathNorm)
};


//! \since YSLib build 433
inline ValuePath
MakeValuePath(std::initializer_list<string> lst)
{
	return {lst, make_unique<ValuePathNorm>()};
}
//! \since YSLib build 433
template<typename... _tParams>
ValuePath
MakeValuePath(_tParams&&... args)
{
	return {make_unique<ValuePathNorm>(), yforward(args)...};
}
#endif

//! \since YSLib build 433
template<typename _tIn>
const ValueNode&
FindChild(const ValueNode& node, _tIn first, _tIn last)
{
	auto p(&node);

	for(; first != last; ++first)
		p = &AccessNode(*p, *first);
	return *p;
}
template<typename _tSeq>
const ValueNode&
FindChild(const ValueNode& node, const _tSeq& name)
{
	return FindChild(node, begin(name), end(name));
}

//! \since YSLib build 433
template<typename _tIn>
const ValueNode&
FindChildNew(const ValueNode& node, _tIn first, _tIn last)
{
	auto p(&node);

	for(; first != last; ++first)
		p = &(*p)[*first];
	return *p;
}
template<typename _tSeq>
const ValueNode&
FindChildNew(const ValueNode& node, const _tSeq& name)
{
	return FindChildNew(node, begin(name), end(name));
}


namespace Windows
{

namespace UI
{

using namespace YSLib::UI;

//! \since YSLib build 427
class DesktopScreen : private WindowReference, public Devices::Screen
{
private:
	//! \since YSLib build 570
	lref<Environment> env;

public:
	DesktopScreen(Environment& = FetchEnvironment());

	using Screen::GetSize;

	//! \since YSLib build 569
	void
	Update(Drawing::ConstBitmapPtr) ynothrow override;
};


/*!
\brief 控件适配器。
\note 引用现有 Win32 控件，不具有所有权。
\since YSLib build 426
*/
class /*YF_API*/ ControlAdaptor : private WindowReference, implements IWidget
{
private:
	//! \note 除非被转移否则非空。
	//@{
	//! \since YSLib build 569
	unique_ptr<AView> view_ptr; //!< 部件视图指针。
	unique_ptr<Renderer> renderer_ptr; //!< 渲染器指针。
	unique_ptr<AController> controller_ptr; //!< 控制器指针。
	//@}

public:
	//! \since YSLib build 547
	explicit
	ControlAdaptor(NativeWindowHandle);
	DefDelCopyCtor(ControlAdaptor)
	//! \since YSLib build 427
	DefDeMoveCtor(ControlAdaptor)
	/*!
	\brief 析构：虚实现。

	自动释放焦点后释放部件资源。
	*/
	virtual
	~ControlAdaptor();

public:
	WidgetRange
	GetChildren() ImplI(IWidget);
	AController&
	GetController() const ImplI(IWidget);
	//! \since YSLib build 547
	using WindowReference::GetNativeHandle;
	DefGetterMem(const ynothrow, SDst, Height, GetView())
	Renderer&
	GetRenderer() const ImplI(IWidget);
	//! \since YSLib build 569
	AView&
	GetView() const ImplI(IWidget);
	DefGetterMem(const ynothrow, SDst, Width, GetView())
	DefGetterMem(const ynothrow, SPos, X, GetView())
	DefGetterMem(const ynothrow, SPos, Y, GetView())

	DefSetterMem(ynothrow, SDst, Height, GetView())
	DefSetterMem(ynothrow, SDst, Width, GetView())
	DefSetterMem(ynothrow, SPos, X, GetView())
	DefSetterMem(ynothrow, SPos, Y, GetView())
};


/*!
\brief 控件适配器迭代器。
\since YSLib build 426
*/
class ControlAdaptorIterator
	: public ystdex::input_iteratable<ControlAdaptorIterator, ControlAdaptor&>
{
public:
	//! \since YSLib build 887
	//@{
	using iterator_category = std::input_iterator_tag;
	using value_type = IWidget;
	using difference_type = ptrdiff_t;
	using pointer = void;
	using reference = ControlAdaptor&;
	//@}

private:
	shared_ptr<ControlAdaptor> control_ptr;

public:
	DefDeCtor(ControlAdaptorIterator)
	//! \since YSLib build 547
	ControlAdaptorIterator(shared_ptr<ControlAdaptor>);
	//! \since YSLib build 547
	ControlAdaptorIterator(NativeWindowHandle);
	DefDeCopyCtor(ControlAdaptorIterator)
	DefDeMoveCtor(ControlAdaptorIterator)

	ControlAdaptorIterator&
	operator++();
	ControlAdaptor&
	operator*() const;

	friend bool
	operator==(const ControlAdaptorIterator&, const ControlAdaptorIterator&);
};

} // namespace UI;

} // namespace Windows;

} // namespace YSLib;

#endif

