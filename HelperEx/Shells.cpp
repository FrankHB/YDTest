#include "Shells.h"
#include YFM_Helper_HostWindow
#include YFM_Helper_HostedUI
#include <windows.h>
#include <sstream>
#include YFM_NPL_SContext
#include <cfloat> // for FLT_EPSILON;
#include YFM_Win32_Helper_Win32Control
#include YFM_YSLib_UI_Button
#include YFM_YSLib_UI_ComboList
#include YFM_YSLib_UI_TabControl

namespace YSLib
{

namespace Host
{

void
PrintFPS(FPSCounter& f, Window& wnd)
{
	const auto t(f.Refresh());

	if(t != 0)
	{
		wchar_t buf[16];

		std::swprintf(buf, size(buf), L"FPS: %u.%03u\n", t / 1000, t % 1000);
		wnd.SetText(buf);
	}
}

} // namespace Host;


namespace Shells
{

} // namespace Shells;

namespace Drawing
{



} // namespace Drawing;

namespace UI
{

void
RegisterControls(WidgetLoader& ldr)
{
	ldr.Default.Register<Widget, Control, Panel, Label, Button, TextList,
		ListBox, TabBar, TabControl>({"Widget", "Control", "Panel", "Label",
		"Button", "TextList", "ListBox", "TabBar", "TabControl"});
	ldr.Bounds.Register<Widget, Control, Panel, Label, Button, TextList,
		ListBox, TabBar, TabControl>({"Widget", "Control", "Panel", "Label",
		"Button", "TextList", "ListBox", "TabBar", "TabControl"});
	ldr.Insert.Register<Panel, TabBar, TabControl>({"Panel", "TabBar",
		"TabControl"}),
	ldr.InsertZOrdered.Register<Panel>({"Panel"});
}

} // namespace UI;

namespace Windows
{

namespace UI
{

DesktopScreen::DesktopScreen(Environment& e)
	: WindowReference(::GetDesktopWindow()), Screen(WindowReference::GetSize()),
	env(e)
{
	YAssert(::IsWindow(GetNativeHandle()), "Invalid window handle found.");
}

void
DesktopScreen::Update(Drawing::ConstBitmapPtr) ynothrow
{
}

namespace
{

//! \since YSLib build 568
::HWND
N_GetWindow(::HWND h_wnd, unsigned int cmd)
{
	::HWND res;

	if(YB_UNLIKELY(!(res = ::GetWindow(h_wnd, cmd))) && ::GetLastError() != 0)
		YCL_Raise_Win32E("GetWindow");
	return res;
}

} // unnamed namespace;

ControlAdaptor::ControlAdaptor(NativeWindowHandle h)
	: WindowReference(h), view_ptr(static_cast<AView*>(new ControlView(h))),
	renderer_ptr(new PseudoRenderer()), controller_ptr(new Controller(true))
{}
ControlAdaptor::~ControlAdaptor()
{}

WidgetRange
ControlAdaptor::GetChildren()
{
	return {ControlAdaptorIterator(N_GetWindow(GetNativeHandle(),
		GW_CHILD | GW_HWNDFIRST)), ControlAdaptorIterator()};
}
AController&
ControlAdaptor::GetController() const
{
	return *controller_ptr;
}
Renderer&
ControlAdaptor::GetRenderer() const
{
	if(YB_UNLIKELY(!renderer_ptr))
		throw LoggedEvent("Null renderer pointer found.");
	return *renderer_ptr;
}
AView&
ControlAdaptor::GetView() const
{
	if(YB_UNLIKELY(!view_ptr))
		throw LoggedEvent("Null view pointer found.");
	return *view_ptr;
}


ControlAdaptorIterator::ControlAdaptorIterator(
	shared_ptr<ControlAdaptor> ctl_ptr)
	: control_ptr(std::move(ctl_ptr))
{}
ControlAdaptorIterator::ControlAdaptorIterator(NativeWindowHandle h)
	: ControlAdaptorIterator(make_shared<ControlAdaptor>(h))
{}
ControlAdaptorIterator&
ControlAdaptorIterator::operator++()
{
	YAssertNonnull(control_ptr);
	try
	{
		// TODO: Cache lookup.
		if(const auto new_h_wnd
			= N_GetWindow(control_ptr->GetNativeHandle(), GW_HWNDNEXT))
			control_ptr.reset(new ControlAdaptor(new_h_wnd));
		else
			control_ptr.reset();
	}
	catch(Win32Exception& e)
	{
		if(YB_LIKELY(e.GetErrorCode() == ERROR_NO_MORE_FILES)
			|| e.GetErrorCode() == ERROR_INVALID_HANDLE)
			control_ptr.reset();
		else
			throw;
	}
	return *this;
}
ControlAdaptor&
ControlAdaptorIterator::operator*() const
{
	YAssertNonnull(control_ptr);
	return *control_ptr;
}

bool
operator==(const ControlAdaptorIterator& x, const ControlAdaptorIterator& y)
{
	return (x.control_ptr == y.control_ptr) || (bool(x.control_ptr)
		&& bool(y.control_ptr) && x.control_ptr->GetNativeHandle()
		== y.control_ptr->GetNativeHandle());
}

} // namespace UI;

} // namespace Windows;

} // namespace YSLib;

#if TEST_GUI == 2
namespace platform_ex
{

inline std::string
ToANSI(const std::wstring& str, int cp = 936)
{
	return WCSToMBCS(str.c_str(), str.length(), cp);
}

string
SToMBCS(String str, int cp)
{
	return platform_ex::WCSToMBCS(reinterpret_cast<const wchar_t*>(str.c_str()),
		str.length(), cp);
}

string
UTF8ToGBK(string str)
{
	return SToMBCS(str, 936);
}


using ystdex::indirect_input_iterator;
typedef ::IShellFolder IShellFolder;
typedef ::IEnumIDList IEnumIDList;
typedef COMPtr<IShellFolder> ShellFolderPtr;
typedef COMPtr<IEnumIDList> EnumIDListPtr;


// 关于内存管理和 OLE32 库的链接：
// http://blogs.msdn.com/b/oldnewthing/archive/2004/07/05/173226.aspx


class YF_API ShellPathFailure : public YSLib::LoggedEvent
{
public:
	ShellPathFailure(const std::string& msg) ynothrow
		: LoggedEvent(msg)
	{}
};


class ShellPath
{
private:
	::LPITEMIDLIST p_idl;

public:
	ShellPath(::LPITEMIDLIST p = nullptr) ynothrow
		: p_idl(p)
	{}
	ShellPath(const ShellPath& pth)
		: p_idl(::ILClone(pth.p_idl))
	{
		if(pth.p_idl && !p_idl)
			throw std::bad_alloc();
	}
	ShellPath(ShellPath&& pth) ynothrow
		: p_idl(pth.p_idl)
	{
		pth.p_idl = nullptr;
	}
	~ShellPath() ynothrow
	{
		::CoTaskMemFree(p_idl);
	}

	ShellPath&
	operator=(ShellPath&& pth) ynothrow
	{
		std::swap(p_idl, pth.p_idl);
		return *this;
	}

	ShellPath&
	operator+=(const ::SHITEMID& id) ynothrow
	{
		if(!::ILAppendID(p_idl, &id, TRUE))
			throw ShellPathFailure("Appending ID failed.");
		return *this;
	}

	::ITEMIDLIST&
	operator*() const ynothrow
	{
		yconstraint(p_idl);

		return *p_idl;
	}

	explicit DefCvt(const ynothrow, bool, bool(p_idl))

	DefGetter(const ynothrow, ::LPITEMIDLIST, , p_idl)
	DefGetter(const, ::ITEMIDLIST&, Object, EnsureNonNull(p_idl), *p_idl)
	DefGetter(ynothrow, ::LPITEMIDLIST&, Ref, p_idl)
};


ShellPath
LocateFolder(int csidl, ::HANDLE token = nullptr)
	ythrow(COMException)
{
	::LPITEMIDLIST p{};

	CheckHResult(::SHGetFolderLocation(nullptr, csidl, token, 0, &p));
	return p;
}

template<typename... _tParams>
ShellPath
GetNext(IEnumIDList& lst, _tParams&&...) ythrow(COMException)
{
	::LPITEMIDLIST p{};

	CheckHResult(lst.Next(1, &p, nullptr));
	return p;
}


ShellFolderPtr
GetDesktopFolder() ythrow(COMException)
{
	IShellFolder* p{};

	CheckHResult(::SHGetDesktopFolder(&p));
	return p;
}

template<typename... _tParams>
ShellFolderPtr
BindToObject(IShellFolder& folder, _tParams&&... args) ythrow(COMException)
{
	IShellFolder* p{};

	CheckHResult(folder.BindToObject(yforward(args)...,
		reinterpret_cast<void**>(&p)));
	return p;
}

ShellFolderPtr
BindPath(IShellFolder& folder, const ShellPath& p_pth,
	::IBindCtx* p_ctx = nullptr)
	ythrow(COMException)
{
	return BindToObject(folder, p_pth.Get(), p_ctx, ::IID_IShellFolder);
}

EnumIDListPtr
EnumObjects(IShellFolder& folder, ::SHCONTF flags = SHCONTF_FOLDERS
	| SHCONTF_NONFOLDERS, ::HWND h_own = nullptr)
	ythrow(COMException)
{
	IEnumIDList* p{};

	CheckHResult(folder.EnumObjects(h_own, flags, &p));
	return p;
}


std::wstring
GetDisplayName(IShellFolder& folder, const ShellPath& p_pth)
{
	::STRRET strret;
	wchar_t buf[MAX_PATH];

	CheckHResult(folder.GetDisplayNameOf(p_pth.Get(), SHGDN_INFOLDER, &strret));
	::StrRetToBufW(&strret, p_pth.Get(), buf, MAX_PATH);
	return buf;
}

ShellPath
ParseDisplayName(IShellFolder& folder, const wchar_t* name,
	::ULONG* p_attr = nullptr)
{
	::LPITEMIDLIST p{};
	std::wstring str(name);

	CheckHResult(folder.ParseDisplayName(nullptr, nullptr, &str[0], nullptr,
		&p, p_attr));
	return p;
}


class YF_API DirectorySession
{
public:
	typedef ShellFolderPtr NativeHandle;

private:
	NativeHandle dir;

protected:
	EnumIDListPtr pList;

public:
//	explicit
//	DirectorySession(const wchar_t* name)
//	{}
	explicit
	DirectorySession(NativeHandle p_folder = {},
		::SHCONTF flags = SHCONTF_FOLDERS | SHCONTF_NONFOLDERS)
		: dir(std::move(p_folder)),
		pList(dir ? ::EnumObjects(*dir, flags) : nullptr)
	{}
	DirectorySession(DirectorySession&& h)
		: dir(std::move(h.dir)), pList(std::move(h.pList))
	{
		yunseq(h.dir = nullptr, pList = nullptr);
	}
	~DirectorySession() ynothrow = default;

	NativeHandle
	GetNativeHandle() const ynothrow
	{
		return dir;
	}

	void
	Rewind() ythrow(COMException)
	{
		if(pList)
			CheckHResult(pList->Reset());
	}
};


class YF_API HDirectory final : private DirectorySession
{
private:
	mutable ShellPath p_path;
	mutable std::string utf8_name;

public:
	template<typename... _tParams>
	explicit
	HDirectory(_tParams&&... args)
		: DirectorySession(yforward(args)...), p_path(), utf8_name()
	{}

	HDirectory&
	operator*() ynothrow
	{
		return *this;
	}
	const HDirectory&
	operator*() const ynothrow
	{
		return *this;
	}

	operator string() const ynothrow
	{
		return GetName();
	}

	HDirectory&
	operator++()
	{
		p_path = GetNext(pList.GetObject());
		return *this;
	}

	explicit
	operator bool() const ynothrow
	{
		return bool(p_path);
	}

	bool
	IsDirectory() const ynothrow
	{
		::ULONG uAttr(SFGAO_FOLDER);

		GetNativeHandle()->GetAttributesOf(1, const_cast< ::LPCITEMIDLIST*>(
			&p_path.GetRef()), &uAttr);
		return uAttr & SFGAO_FOLDER;
	}

	const char*
	GetName() const ynothrow
	{
		if(p_path)
		{
			try
			{
				const auto h(GetNativeHandle());

				yassume(h);

				utf8_name = ToANSI(GetDisplayName(*h, p_path));
				return &utf8_name[0];
			}
			catch(...)
			{}
		}
		return ".";
	}
	using DirectorySession::GetNativeHandle;
	const ShellPath&
	GetPathPtr() const ynothrow
	{
		return p_path;
	}
	ShellPath&
	GetPathPtrRef() ynothrow
	{
		return p_path;
	}

	using DirectorySession::Rewind;
};

typedef indirect_input_iterator<HDirectory*> FileIterator;

}
#endif

