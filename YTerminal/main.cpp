﻿/*
	© 2013, 2015, 2021 FrankHB.

	This file is part of the YSLib project, and may only be used,
	modified, and distributed under the terms of the YSLib project
	license, LICENSE.TXT.  By continuing to use, modify, or distribute
	this file you indicate that you have read the license and
	understand and accept it fully.
*/

/*!	\file main.cpp
\ingroup YTerinal
\brief 主界面。
\version r154
\author FrankHB <frankhb1989@gmail.com>
\since YSLib build 435
\par 创建时间:
	2013-08-05 19:06:11 +0800
\par 修改时间:
	2021-02-17 23:43 +0800
\par 文本编码:
	UTF-8
\par 模块名称:
	YTerminal::Main
*/


#include <ysbuild.h>
#include YFM_Helper_HostedUI
#include YFM_Win32_YCLib_MinGW32
#include <atomic>
#include <thread>
#include <chrono>
#include <iostream>

namespace YTerminal
{

using namespace YSLib;
using namespace Drawing;

class Cell
{
public:
	char32_t Char;
	Color Foreground;
	Color Background;
	std::uint8_t flags;
};

////

////

} // namespace YTerminal;


int
main()
{
	::SECURITY_ATTRIBUTES sa{sizeof(::SECURITY_ATTRIBUTES), nullptr, true};
	::HANDLE h_read, h_write_slave;
	::HANDLE h_read_slave, h_write;

	if(YB_UNLIKELY(!::CreatePipe(&h_read, &h_write_slave, &sa, 0)))
		YCL_Raise_Win32E("CreatePipe");
	if(YB_UNLIKELY(!::SetHandleInformation(h_read, HANDLE_FLAG_INHERIT, 0)))
		YCL_Raise_Win32E("SetHandleInformation");
	if(YB_UNLIKELY(!::CreatePipe(&h_read_slave, &h_write, &sa, 0)))
		YCL_Raise_Win32E("CreatePipe");
	if(YB_UNLIKELY(!::SetHandleInformation(h_write, HANDLE_FLAG_INHERIT, 0)))
		YCL_Raise_Win32E("SetHandleInformation");

	::STARTUPINFOW si;
	::GetStartupInfoW(&si);

	si.hStdInput = h_read_slave;
	si.hStdError = h_write_slave;
	si.hStdOutput = h_write_slave;
	si.wShowWindow = SW_HIDE;
	si.dwFlags = STARTF_USESHOWWINDOW | STARTF_USESTDHANDLES;

	::PROCESS_INFORMATION pi;
	const wchar_t name[]{L"C:\\windows\\system32\\cmd.exe"};
	wchar_t opt[]{L""};

	if(YB_UNLIKELY(!CreateProcessW(name, opt, {}, {}, true, 0, {}, {}, &si,
		&pi) && ::GetLastError() != 0))
		YCL_Raise_Win32E("CreateProcess");

	std::atomic<bool> app_exit{};
	std::thread handle_incoming([&]{
        while(!app_exit)
		{
			std::string str;
			::DWORD bWrite;

			getline(std::cin, str);
			if(ystdex::trim(str) == "exit")
				app_exit = true;
			str += '\n';
			::WriteFile(h_write, str.c_str(), str.size(), &bWrite, {});
		}
	});

	while(!app_exit)
	{
		char obuf[4096]{};
		::DWORD bRead;

		if(::ReadFile(h_read, obuf, 4095, &bRead, {}))
			std::cout << obuf;
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
	}
	handle_incoming.join();
	::CloseHandle(h_write);
	::CloseHandle(h_read_slave);
	::CloseHandle(h_read);
	::CloseHandle(h_write_slave);
#if 0
	using namespace YSLib;
	using namespace Drawing;
	using namespace Host;
	using namespace UI;

	GUIApplication app;
	Control ctl({0, 0, 100, 100});

	ShowTopLevelDraggable(ctl);
	GetWindowPtrOf(ctl)->SetOpacity(0xC0);
	yunseq(
		FetchEvent<Click>(ctl) += [](CursorEventArgs&& e){
			if(e.GetKeys()[VK_RBUTTON])
				YSLib::PostQuitMessage(0);
		}
	);
	Execute(app);
#endif
}

