#include <ysbuild.h>
#include YFM_Helper_GUIApplication
#include YFM_Helper_HostedUI
#include YFM_Helper_HostWindow
#include YFM_YSLib_Service_ContentType
#include "../HelperEx/Shells.h"

//#include "../HelperEx/ControlAdaptor.h"
#include YFM_NPL_SContext //-
#include <iostream>
#include <fstream>
#include <sstream>
#include YFM_YSLib_UI_TextBox

using namespace YSLib;
using namespace Drawing;
using namespace Host;
using namespace Shells;
using namespace UI;


#define YSL_ChuBaka_Shl 1

#if YSL_ChuBaka_Shl
class Shl : public AnimatedShell
{
public:
	std::function<void()> Paint;
	Widget& Main;

	template<typename F>
	Shl(Widget& wgt, F&& f)
		: AnimatedShell(yforward(f)), Main(wgt)
	{
		IdleSleep = {};
	}

	void
	OnPaint() override
	{
		PrintFPS(fpsCounter, *GetWindowPtrOf(Main));
	}
};
#endif
//-{
namespace test
{

using namespace std;
using namespace ystdex;

} // namespace test;


namespace ystdex
{

namespace details
{

} // namespace details;


} // namespace ystdex;
//-}


int
main()
{
	GUIApplication app;
	Panel pnl(Size(640, 480));
	Label lbl({100, 100, 400, 100});
	Label lblx({100, 300, 320, 100});
	CheckButton cb({100, 280, 104, 22});
	TextBox tb({100, 200, 400, 48});
	TextPlaceholder tp;

	using namespace YSLib::Windows::UI;
	using namespace std;
	using namespace ystdex;

	lbl.Font.SetSize(48),
	lblx.Font.SetSize(24),
//	tb.Font.SetSize(36),
	yunseq(
	lbl.Text = u"你好，世界←_←", lbl.ForeColor = ColorSpace::Blue,
	lblx.Text = u"⑨chu大笨蛋！→_→\n",
	lblx.Background = SolidBrush(ColorSpace::Yellow),
	lblx.AutoWrapLine = true,
	lblx.ForeColor = ColorSpace::Green,
	cb.Text = u"显示文本框内容",
	cb.Ticked += [&](CheckBox::TickedArgs&& e){
		(tp.GetCapturedPtr() ? tp.MaskChar : tb.MaskChar)
			= e ? char32_t() : u'●';
		Invalidate(tb);
	},
	tb.Text = u"文本框测试文本",
	tb.MaxLength = 5,
	tb.MaskChar = u'●',
	FetchEvent<KeyUp>(tb) += [&]{
		lbl.Text = tb.Text;
		Invalidate(lbl);
		Invalidate(tb);
	}
	);
	tp.Font.SetSize(32);
	tp.Font.SetStyle(FontStyle::Italic);
	tp.Text = "请输入文本";
	tp.BindByFocus(tb, std::bind(&TextPlaceholder::SwapTextBox<TextBox>,
		std::placeholders::_1, std::placeholders::_2));
	lblx.Text *= 3;
	AddWidgets(pnl, lbl, lblx, cb, tb);
	ShowTopLevel(pnl, WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX, 0);
#if YSL_ChuBaka_Shl
	Execute(app, make_shared<Shl>(pnl, [&](UI::PaintEventArgs&&){
		PostMessage<SM_Task>(0x20, [&]{Invalidate(pnl);});
	}));
#else
	Execute(app);
#endif
}

